/*
 * This file is part of moss-jobs
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.jobs.systemprocessor;

/**
 * A SystemProcessor can either run interleaved within the current
 * execution context (main loop) or branched as an entirely separate
 * thread.
 */
enum ProcessorMode
{
    Main,
    Branched,
}

/**
 * Each SystemProcessor can currently be in an available or busy state.
 */
enum ProcessorStatus
{
    /**
     * Available now to grab a new job
     */
    Available,

    /**
     * Busy now executing another job
     */
    Busy,
}

/**
 * A RunCondition is used for Branched processors
 */
alias RunCondition = bool delegate();

/**
 * A SystemProcessor is created for each job or subsystem handler.
 * For example, systems may run concurrently or outside the main
 * event loop (branched)
 */
public abstract class SystemProcessor
{
    /**
     * Super constructor that every processor must abide by to
     * ensure correct initialisation
     */
    this(const(string) processorName, ProcessorMode mode = ProcessorMode.Main)
    {
        this.processorName = processorName;
        _mode = mode;
    }

    /**
     * Return the name of this system processor
     */
    pragma(inline, true) pure @property const(string) processorName() @safe @nogc nothrow
    {
        return _processorName;
    }

    /**
     * Return the processing mode for this system processor.
     */
    pragma(inline, true) pure @property ProcessorMode mode() @safe @nogc nothrow
    {
        return _mode;
    }

    /**
     * Return the current status
     */
    pragma(inline, true) pure @property ProcessorStatus status() @safe @nogc nothrow
    {
        return _status;
    }

    /**
     * Implementations must return true if they found work, otherwise false to go
     * back to idly waiting on a job
     */
    abstract bool allocateWork();

    /**
     * Implementations must override this method to perform the work that was
     * retrieved during the allocateWork routine. The main work may be performed
     * outside of the main thread so it is imperative any data access here is
     * thread-safe.
     */
    abstract void performWork();

    /**
     * Implementations must override this method to synchronize data safely
     * with other mechanisms when their work has completed.
     */
    abstract void syncWork();

    /**
     * Implementations may override this method for custom stop functionality,
     * however it is very uncommon so we don't require it.
     */
    void stop()
    {

    }

    /**
     * Update the current status
     */
    pragma(inline, true) pure @property void status(ProcessorStatus ps) @safe @nogc nothrow
    {
        _status = ps;
    }

private:

    /**
     * Update the static processor name
     */
    pure @property void processorName(const(string) inp) @safe @nogc nothrow
    {
        _processorName = inp;
    }

    string _processorName = null;
    ProcessorMode _mode = ProcessorMode.Main;
    ProcessorStatus _status = ProcessorStatus.Available;
    RunCondition _runc = null;
}
