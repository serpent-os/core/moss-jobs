/*
 * This file is part of moss-jobs
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.jobs.worker;

public import std.stdint : uint8_t;
public import std.concurrency : Tid;
public import moss.jobs.systemprocessor : SystemProcessor;

import std.concurrency : thisTid, send, receive;
import moss.jobs.messaging;
import core.atomic : atomicLoad, atomicStore;
import std.exception : enforce;

/**
 * Track current status of the worker to allow manipulation into new states
 */
enum WorkerState : uint8_t
{
    /**
     * The Worker is currently starting
     */
    Starting = 0,

    /**
     * The Worker is currently idle, awaiting work
     */
    Idle,

    /**
     * The worker is now allocating work and must be allowed to complete.
     */
    AllocatingWork,

    /**
     * We're now ready to *begin* work.
     */
    ReadyForWork,

    /**
     * The worker is now running the work() routine and must be allowed to
     * complete.
     */
    Working,

    /**
     * The worker has finished the work() routine and is now ready to be
     * synced.
     */
    FinishedWork,

    /**
     * The worker is now syncing the work and must be allowed to complete.
     */
    SyncingWork,

    /**
     * The worker is stopped, awaiting a startup
     */
    Stopped,
}

/**
 * A Worker implementation is responsible for managing the entire lifecycle of
 * a SystemProcessor implementation
 */
public abstract class Worker
{

    @disable this();

    /**
     * Super constructor to ensure ParentTID + Processor properties are set
     */
    this(Tid parentTid, SystemProcessor processor) @safe @nogc nothrow
    {
        _parentTid = parentTid;
        _processor = processor;
    }

    /**
     * Return the currently known worker state
     */
    pure @property WorkerState workerState() @safe @nogc nothrow
    {
        return atomicLoad(_state);
    }

    /**
     * Return the parent thread ID
     */
    pure @property Tid parentTid() @safe @nogc nothrow
    {
        return _parentTid;
    }

    /**
     * Return our own Tid
     */
    pure @property Tid ourTid() @safe @nogc nothrow
    {
        return _ourTid;
    }

    /**
     * Return the SystemProcessor
     */
    pure @property SystemProcessor processor() @safe @nogc nothrow
    {
        return _processor;
    }

    /**
     * Perform the startup routine, optionally block until complete
     */
    abstract void startup(bool blocking);

    /**
     * Execute shutdown routine, optionally block until complete
     */
    abstract void shutdown(bool blocking);

    /**
     * Allocate the work unit if possible. A busy processor should be allowed
     * to complete its work
     */
    abstract void allocateWork(bool blocking);

    /**
     * Perform the work if not yet performed. If working, "ping" to ensure the
     * work continues.
     */
    abstract void performWork(bool blocking);

    /**
     * Synchronize any finished work to mutable data systems.
     */
    abstract void syncWork(bool blocking);

package:

    /**
     * Main message handling loop for our workers
     */
    final void runnable()
    {
        /* Transition to starting state */
        workerState = WorkerState.Starting;

        _ourTid = thisTid();

        /* Notify startup if requested */
        if (blockStartup)
        {
            send(parentTid, StartControlAck());
        }

        workerState = WorkerState.Idle;

        /* Run until told to stop */
        while (workerState != WorkerState.Stopped)
        {
            receive((QuitControlMessage m) {

                /* Handle shutdown gracefully */
                processor.stop();
                workerState = WorkerState.Stopped;

                if (m.blocking)
                {
                    send(parentTid, QuitControlAck());
                }
            }, (AllocateWorkControlMessage m) {
                /* Handle work allocation */
                enforce(workerState == WorkerState.AllocatingWork, "Cannot allocateWork()");
                const bool workAllocated = processor.allocateWork();

                if (workAllocated)
                {
                    /* Transition into waiting for work */
                    workerState = WorkerState.ReadyForWork;
                }
                else
                {
                    /* Transition to job claiming again */
                    workerState = WorkerState.Idle;
                }

                if (m.blocking)
                {
                    send(parentTid, AllocateWorkControlAck());
                }
            }, (PerformWorkControlMessage m) {
                /* Handle work perform */
                enforce(workerState == WorkerState.Working, "Cannot work()");

                processor.performWork();

                /* Transition to work completion, pending syncing */
                workerState = WorkerState.FinishedWork;

                if (m.blocking)
                {
                    send(parentTid, PerformWorkControlAck());
                }
            }, (SyncControlMessage m) {
                /* Handle sync allocation */

                enforce(workerState == WorkerState.SyncingWork, "Cannot sync()");
                processor.syncWork();

                /* Transition back to waiting for work again */
                workerState = WorkerState.Idle;

                if (m.blocking)
                {
                    send(parentTid, SyncControlAck());
                }
            });
        }
    }

    /**
     * Update the new workerState
     */
    pure @property void workerState(WorkerState newState) @safe @nogc nothrow
    {
        _state.atomicStore(newState);
    }

    /**
     * Return the blockstartup property
     */
    pure @property bool blockStartup() @safe @nogc nothrow
    {
        return _blockStartup;
    }

    /**
     * Update the blockstartup property
     */
    pure @property void blockStartup(bool b) @safe @nogc nothrow
    {
        _blockStartup = b;
    }

private:

    Tid _parentTid;
    Tid _ourTid;
    SystemProcessor _processor;
    WorkerState _state = WorkerState.Stopped;
    bool _blockStartup = false;
}
