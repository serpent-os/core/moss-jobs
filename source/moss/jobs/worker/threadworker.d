/*
 * This file is part of moss-jobs
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.jobs.worker.threadworker;

public import moss.jobs.worker;
import moss.jobs.messaging;
import std.exception : enforce;
import std.concurrency : receiveOnly, receive, send;

import core.thread.osthread;

/**
 * A Worker implementation is responsible for managing the entire lifecycle of
 * a SystemProcessor implementation
 */
final class ThreadWorker : Worker
{

    @disable this();

    /**
     * Super constructor to ensure parentTid + Processor properties are set
     */
    this(Tid parentTid, SystemProcessor processor) @safe nothrow
    {
        super(parentTid, processor);
        systemThread = new Thread(&runnable);
    }

    /**
     * Perform the startup routine, optionally block until complete
     */
    override void startup(bool blocking)
    {
        enforce(workerState == WorkerState.Stopped, "Cannot start a running worker");

        blockStartup = blocking;
        systemThread.start();

        /* Await a reply */
        if (blocking)
        {
            receiveOnly!StartControlAck();
        }
    }

    /**
     * Execute shutdown routine, optionally block until complete
     */
    override void shutdown(bool blocking)
    {
        enforce(workerState != WorkerState.Stopped, "Cannot stop a stopped worker");

        send(ourTid, QuitControlMessage(blocking));
        if (blocking)
        {
            receiveOnly!QuitControlAck;
        }
    }

    /**
     * Allocate the work unit if possible. A busy processor should be allowed
     * to complete its work
     */
    override void allocateWork(bool blocking)
    {
        if (workerState != WorkerState.Idle)
        {
            return;
        }

        workerState = WorkerState.AllocatingWork;
        send(ourTid, AllocateWorkControlMessage(blocking));
        if (blocking)
        {
            receiveOnly!AllocateWorkControlAck;
        }
    }

    /**
     * Perform the work if not yet performed. If working, "ping" to ensure the
     * work continues.
     */
    override void performWork(bool blocking)
    {
        if (workerState != WorkerState.ReadyForWork)
        {
            return;
        }

        workerState = WorkerState.Working;
        send(ourTid, PerformWorkControlMessage(blocking));
        if (blocking)
        {
            receiveOnly!PerformWorkControlAck;
        }
    }

    /**
     * Synchronize any finished work to mutable data systems.
     */
    override void syncWork(bool blocking)
    {
        if (workerState != WorkerState.FinishedWork)
        {
            return;
        }

        workerState = WorkerState.SyncingWork;
        send(ourTid, SyncControlMessage(blocking));
        if (blocking)
        {
            receiveOnly!SyncControlAck;
        }
    }

private:

    Thread systemThread;
}
