/*
 * This file is part of moss-jobs
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.jobs.worker.mainworker;

public import moss.jobs.worker;
import std.exception : enforce;

/**
 * Manage lifecycle of SystemProcessor using main thread.
 */
final class MainWorker : Worker
{

    @disable this();

    /**
     * Super constructor to ensure parentTid + Processor properties are set
     */
    this(Tid parentTid, SystemProcessor processor) @system nothrow
    {
        super(parentTid, processor);
    }

    /**
     * Perform the startup routine
     */
    override void startup(bool blockingIgnored)
    {
        enforce(workerState == WorkerState.Stopped, "Cannot start a running worker");
        workerState = WorkerState.Idle;
    }

    /**
     * Execute shutdown routine, optionally block until complete
     */
    override void shutdown(bool blockingIgnored)
    {
        enforce(workerState != WorkerState.Stopped, "Cannot stop a stopped worker");
        processor.stop();
        workerState = WorkerState.Stopped;
    }

    /**
     * Allocate the work unit if possible. A busy processor should be allowed
     * to complete its work
     */
    override void allocateWork(bool blockingIgnored)
    {
        if (workerState != WorkerState.Idle)
        {
            return;
        }

        workerState = WorkerState.AllocatingWork;
        if (processor.allocateWork())
        {
            workerState = WorkerState.ReadyForWork;
        }
        else
        {
            workerState = WorkerState.Idle;
        }
    }

    /**
     * Perform the work if not yet performed. If working, "ping" to ensure the
     * work continues.
     */
    override void performWork(bool blockingIgnored)
    {
        if (workerState != WorkerState.ReadyForWork)
        {
            return;
        }
        workerState = WorkerState.Working;
        processor.performWork();
        workerState = WorkerState.FinishedWork;
    }

    /**
     * Synchronize any finished work to mutable data systems.
     */
    override void syncWork(bool blockingIgnored)
    {
        if (workerState != WorkerState.FinishedWork)
        {
            return;
        }

        workerState = WorkerState.SyncingWork;
        processor.syncWork();
        workerState = WorkerState.Idle;
    }
}
