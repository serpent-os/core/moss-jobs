/*
 * This file is part of moss-jobs.
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.jobs.jobsystem;

import core.sync.mutex;
import std.stdint : uint64_t;
import serpent.ecs;

import moss.jobs.mainloop : mainLoop;

/**
 * Allow more accurately describing a job component..
 */
public alias Job = serpentComponent;

/**
 * Document the current job status for the system
 */
public enum JobStatus
{
    Unclaimed = 0,
    InProgress = 1,
    Completed = 2,
    Failed = 3,
}

/**
 * Assign an ID to every job
 */
@serpentComponent public struct JobIDComponent
{
    /** ID for this job */
    uint64_t jobID = 0;
}

/**
 * Record status along with each job
 */
@serpentComponent public struct JobStatusComponent
{
    /**
     * Keep track of the current job status.
     */
    JobStatus status;
}

public alias JobCallback = void delegate();

/**
 * We add callbacks into the table for completion hooks.
 * Thread-safety cannot be provided, do so yourself.
 */
@serpentComponent public struct JobCallbackComponent
{
    JobCallback onSuccess = null;
    JobCallback onFailure = null;
};

/**
 * The JobSystem is responsible for storing and managing jobs and their lifetimes.
 * Through the (over)use of locks we ensure threadsafe access to each Job, which
 * is implemented defined.
 *
 * Users of the API can pushJob and finishJob with a given status, and the JobSystem
 * will take care of the internals.
 */
public final class JobSystem
{

    /**
     * Construct a new JobSystem
     */
    this(EntityManager entityManager)
    {
        this.entityManager = entityManager;
        idMutex = new shared Mutex();
        entityManager.registerComponent!JobIDComponent;
        entityManager.registerComponent!JobStatusComponent;
        entityManager.registerComponent!JobCallbackComponent;
        accessMutex = new shared Mutex();

        /**
         * On every main loop tick, GC old jobs and 
         * update the ECS automatically.
         */
        mainLoop.idleAdd(() => { gcJobs(); this.entityManager.step(); }());
    }

    /**
     * Close this JobSystem resources
     */
    void close()
    {
        gcJobs();
    }

    ~this()
    {
        close();
    }

    /**
     * Register the given job type with the system for filtering
     */
    void registerJobType(T)()
    {
        entityManager.registerComponent!T;
    }

    /**
     * Insert a job into the system ensuring archetype lock
     */
    void pushJob(T)(T job, JobCallback onSuccess = null, JobCallback onFailure = null)
    {
        scope (exit)
        {
            accessMutex.unlock_nothrow();
        }
        accessMutex.lock_nothrow();

        auto view = View!ReadWrite(entityManager);
        auto ent = entityManager.create();
        view.addComponent(ent, JobIDComponent(nextJobID));
        view.addComponent(ent, JobStatusComponent(JobStatus.Unclaimed));
        view.addComponent(ent, JobCallbackComponent(onSuccess, onFailure));
        view.addComponent(ent, job);
        entityManager.step();
    }

    /**
     * Attempt to claim a job from the system with the given type
     * This may fail if no jobs are available. Each dedicated processor
     * should try to grab a job and process within the current iteration
     */
    bool claimJob(T)(out JobIDComponent jobID, out T jobPointer)
    {
        scope (exit)
        {
            accessMutex.unlock_nothrow();
        }
        accessMutex.lock_nothrow();

        import std.range : take;
        import std.algorithm : filter;

        jobPointer = T.init;
        jobID = JobIDComponent.init;

        auto view = View!ReadWrite(entityManager);
        /* entityID, jobID, jobStatus, T */
        auto search = view.withComponents!(JobIDComponent, JobStatusComponent, T)
            .filter!((t) => t[2].status == JobStatus.Unclaimed);
        if (search.empty)
        {
            return false;
        }

        auto item = search.take(1).front;
        view.data!JobStatusComponent(item[0].id).status = JobStatus.InProgress;
        jobID = *item[1];
        jobPointer = *item[3];
        entityManager.step();
        return true;
    }

    /**
     * Mark a job as finished for whichever reason
     */
    void finishJob(uint64_t jobID, JobStatus status)
    {
        import std.exception : enforce;

        JobCallback onComplete = null;
        JobCallback onFailure = null;

        scope (exit)
        {
            accessMutex.unlock_nothrow();
            entityManager.step();

            if (status == JobStatus.Completed && onComplete !is null)
            {
                mainLoop.idleAdd(onComplete);
            }
            else if (status == JobStatus.Failed && onFailure !is null)
            {
                mainLoop.idleAdd(onFailure);
            }
        }
        accessMutex.lock_nothrow();

        import std.algorithm : filter;
        import std.range : take;

        auto view = View!ReadWrite(entityManager);
        /* entityID, jobID, jobStatus */
        auto search = view.withComponents!(JobIDComponent, JobStatusComponent,
                JobCallbackComponent)
            .filter!((t) => t[1].jobID == jobID);

        enforce(!search.empty, "JobSystem.finishJob(): Cannot finish job");
        if (search.empty)
        {
            return;
        }

        auto item = search.take(1).front;
        view.data!JobStatusComponent(item[0].id).status = status;
        onComplete = item[3].onSuccess;
        onFailure = item[3].onFailure;
    }

    /**
     * Actively collect completed jobs. Cycle start.
     */
    void gcJobs()
    {
        scope (exit)
        {
            accessMutex.unlock_nothrow();
        }
        accessMutex.lock_nothrow();

        import std.algorithm : filter, each;

        auto view = View!ReadWrite(entityManager);
        view.withComponents!(JobIDComponent, JobStatusComponent)
            .filter!((t) => t[2].status == JobStatus.Completed || t[2].status == JobStatus.Failed)
            .each!((t) => { view.killEntity(t[0].id); }());
    }

    /**
     * Return true if job exists
     */
    bool hasJobs()
    {
        scope (exit)
        {
            accessMutex.unlock_nothrow();
        }
        accessMutex.lock_nothrow();

        import std.algorithm : filter;

        auto search = View!ReadOnly(entityManager).withComponents!(JobIDComponent,
                JobStatusComponent)
            .filter!((t) => t[2].status != JobStatus.Failed && t[2].status != JobStatus.Completed);
        return !search.empty;
    }

private:

    /**
     * Lock and increment the next job ID
     *
     * TODO: Atomics?
     */
    @property uint64_t nextJobID()
    {
        scope (exit)
        {
            idMutex.unlock_nothrow();
        }

        idMutex.lock_nothrow();
        if (jobID + 1 >= uint64_t.max)
        {
            jobID = 1;
        }
        else
        {
            ++jobID;
        }
        return jobID;
    }

    EntityManager entityManager = null;
    shared Mutex idMutex = null;
    uint64_t jobID = 0;
    shared Mutex accessMutex = null;
}
