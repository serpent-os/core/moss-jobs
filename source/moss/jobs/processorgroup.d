/*
 * This file is part of moss-jobs.
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.jobs.processorgroup;

import std.exception : enforce;
import moss.jobs.worker;
import moss.jobs.worker.threadworker;
import moss.jobs.worker.mainworker;
import core.sync.mutex;
import std.concurrency : thisTid, ownerTid, receiveOnly;
import std.algorithm : canFind, remove, each;
import std.range : chain;
import std.string : format;
import moss.jobs.messaging;

public import moss.jobs.systemprocessor;

/**
 * SystemProcessors can be added to a ProcessorGroup to ensure they're run within
 * the same series of executions, which can be useful for ordering their execution
 * within the main loop.
 *
 * Each ProcessorGroup may only be appended to, and explicitly owns each fiber
 * and thread for execution
 */
public final class ProcessorGroup
{

    /**
     * Construct a new ProcessorGroup with the given name
     */
    this(const(string) name)
    {
        enforce(name !is null, "Each ProcessorGroup requires a name");
        this.groupName = name;
        accessMutex = new shared Mutex();
    }

    /**
     * Return the name for this ProcessorGroup
     */
    pure @property const(string) groupName() @safe @nogc nothrow
    {
        return _name;
    }

    /**
     * Add a new SystemProcessor to the list
     */
    void append(SystemProcessor proc)
    {
        scope (exit)
        {
            accessMutex.unlock_nothrow();
        }
        accessMutex.lock_nothrow();

        /* Create an appropriate backing worker */
        final switch (proc.mode)
        {
        case ProcessorMode.Branched:
            branchedWorkers ~= new ThreadWorker(thisTid(), proc);
            break;
        case ProcessorMode.Main:
            mainWorkers ~= new MainWorker(thisTid(), proc);
            break;
        }
    }

    /**
     * Remove a SystemProcessor from this ProcessorGroup. This may only be done
     * from the main thread.
     */
    void remove(SystemProcessor proc)
    {
        scope (exit)
        {
            accessMutex.unlock_nothrow();
        }
        accessMutex.lock_nothrow();

        final switch (proc.mode)
        {
        case ProcessorMode.Branched:
            enforce(branchedWorkers.canFind!((w) => w.processor == proc),
                    "ProcessorGroup.remove(): Cannot find SystemProcessor(%s) for removal".format(
                        proc.processorName));
            branchedWorkers = branchedWorkers.remove!((w) => w.processor == proc);
            break;
        case ProcessorMode.Main:
            enforce(mainWorkers.canFind!((w) => w.processor == proc),
                    "ProcessorGroup.remove(): Cannot find SystemProcessor(%s) for removal".format(
                        proc.processorName));
            mainWorkers = mainWorkers.remove!((w) => w.processor == proc);
            break;
        }
    }

package:

    void allocateWork()
    {
        chain(mainWorkers, branchedWorkers).each!((w) => w.allocateWork(true));
    }

    void performWork()
    {
        chain(mainWorkers, branchedWorkers).each!((w) => w.performWork(false));
    }

    void syncWork()
    {
        chain(mainWorkers, branchedWorkers).each!((w) => w.syncWork(true));
    }

    /**
     * Safely start this ProcessorGroup (fibers + threads)
     */
    void start()
    {
        /* Start all fibers */
        mainWorkers.each!((w) => { w.startup(true); }());

        /* Start all threads */
        branchedWorkers.each!((w) => { w.startup(true); }());
    }

    /**
     * Completely stop this ProcessorGroup from further execution
     */
    void stop()
    {
        /* Stop all threads now to return resources */
        branchedWorkers.each!((w) => w.shutdown(true));

        /* Stop all fibers */
        mainWorkers.each!((w) => w.shutdown(true));

        /* clear all registered workers */
        branchedWorkers = [];
        mainWorkers = [];
    }

private:

    /**
     * Update the name property
     */
    pure @property void groupName(const(string) inp) @safe @nogc nothrow
    {
        _name = inp;
    }

    string _name = null;
    Worker[] branchedWorkers;
    Worker[] mainWorkers;
    shared Mutex accessMutex = null;
}
