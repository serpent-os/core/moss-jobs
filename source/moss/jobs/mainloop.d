/*
 * This file is part of moss-jobs.
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.jobs.mainloop;

public import moss.jobs.idleprocessor;
public import moss.jobs.processorgroup;

import core.sync.mutex;
import core.time;
import core.thread.osthread;
import std.algorithm : each, canFind, remove;
import std.concurrency : initOnce, thisTid, ownerTid;
import std.exception : enforce;
import std.string : format;

/**
 * The MainLoop provides a multiplexing main loop routine for applications
 * that need to run for an indetermine (or permanent) duration of time.
 * 
 * The loop provides a blocking `.run()` call which may only ever be called once
 * for the lifecycle of an app, and an associated `.quit()` method. To add
 * any lifetime functionality, one must use processors and processor groups
 * to hook into the main routine.
 *
 * A default `systemGroup()` is provided with an integrated `IdleCallback` system
 * processor, allowing idle functions to run lazily on a dedicated fiber away from
 * the main execution context.
 *
 * For parallel work outliving the main loop, you should add a Branched processor
 * to a group to ensure it executes separately, and will be updated constantly
 * with work should the runCondition() be met and the processor be in an
 * available state.
 */
public final class MainLoop
{
    /**
     * Run the event loop and block until completion, but avoid busy-spinning
     * like crazy and instead go to sleep for a reasonable duration (set up in
     * the MainLoop constructor) before checking for runnable tasks again.
     */
    void run()
    {
        if (running)
        {
            Thread.sleep(_busySpinDuration);
            return;
        }

        running = true;

        /* Start groups */
        groups.each!((g) => g.start());

        /* Run groups */
        while (running)
        {
            /* Break execution next cycle */
            if (shouldQuit)
            {
                running = false;
                break;
            }
            groups.each!((g) => g.allocateWork());
            groups.each!((g) => g.performWork());
            groups.each!((g) => g.syncWork());
        }

        groups.each!((g) => g.stop());
    }

    /**
     * Return running flag
     */
    pragma(inline, true) pure @property bool running() @safe @nogc nothrow
    {
        return _running;
    }

    /**
     * Shorthand, add a callback to the IdleProcessor
     */
    CallbackID idleAdd(NonRecurringIdleCallback cb)
    {
        return idleProc.addCallback(cb);
    }

    /**
     * Shorthand, add a callback to the IdleProcessor
     */
    CallbackID idleAdd(IdleCallback cb)
    {
        return idleProc.addCallback(cb);
    }

    /**
     * Quit the loop
     */
    void quit()
    {
        shouldQuit = true;
    }

    /**
     * Return the systemGroup, instead of needing a new one
     */
    pure @property ProcessorGroup systemGroup() @safe @nogc nothrow
    {
        return _systemGroup;
    }

    /**
     * Append a Group to the current list of groups, so that it executes at the
     * very end of the current set
     */
    void appendGroup(ProcessorGroup group)
    {
        scope (exit)
        {
            accessMutex.unlock_nothrow();
        }
        accessMutex.lock_nothrow();

        groups ~= group;
    }

    /**
     * Remove a group from the execution set
     */
    void removeGroup(ProcessorGroup group)
    {
        scope (exit)
        {
            accessMutex.unlock_nothrow();
        }
        accessMutex.lock_nothrow();

        enforce(groups.canFind!((g) => g == group),
                "MainLoop.removeGroup(): Cannot find ProcessorGroup(%s)".format(group.groupName));
        groups = groups.remove!((g) => g == group);
    }

private:

    /**
     * Modify running flag
     */
    pure @property void running(bool b) @safe @nogc nothrow
    {
        _running = b;
    }

    /**
     * Only our helper `loop()` can create an MainLoop, ensuring there is only
     * one present within an application instance.
     */
    this(const(ushort) busySpinFrequencyHz)
    {
        accessMutex = new shared Mutex();

        /* Define system group with IdleProcessor first */
        idleProc = new IdleProcessor();
        _systemGroup = new ProcessorGroup("system");
        _systemGroup.append(idleProc);
        groups = [_systemGroup];
        /* This needs to be a floating point type, otherwise it may get rounded to 0 during division */
        immutable float _frameTimeDurationUsecs = 1_000_000 / busySpinFrequencyHz;
        /** Thread.sleep needs a Duration and converting a usec value gives sufficient precision */
        immutable _busySpinDuration = dur!"usecs"(cast(ulong) _frameTimeDurationUsecs);
    }

    bool _running = false;
    IdleProcessor idleProc = null;
    Duration _busySpinDuration = Duration.zero;

    /* Our runnable groups */
    ProcessorGroup[] groups;
    ProcessorGroup _systemGroup = null;
    shared Mutex accessMutex = null;
    bool shouldQuit = false;
}

/**
 * Static instance.
 */
private __gshared MainLoop loopInstance = null;

/**
 * Ensure there is only ever one instance of the loop.
 *
 * Instead of busy-spinning wildly when tasks are running, set a
 * reasonable default busy-spin frequency in the range [1;1000] Hz.
 */
public MainLoop mainLoop(const(ushort) busySpinFrequencyHz = 100)
{
    enforce(busySpinFrequencyHz > 0 && busySpinFrequencyHz <= 1000,
            "busySpinFrequencyHz (= %s Hz) _MUST_ be in the range [1;1000] Hz".format(
                busySpinFrequencyHz));

    return initOnce!loopInstance(new MainLoop(busySpinFrequencyHz));
}

/**
 * Always free the loop instance during the module destructor
 */
shared static ~this()
{
    if (loopInstance !is null)
    {
        loopInstance.destroy();
        loopInstance = null;
    }
}
