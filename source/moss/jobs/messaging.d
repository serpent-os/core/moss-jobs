/*
 * This file is part of moss-jobs
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.jobs.messaging;

/**
 * StartControlAck is sent immediately after entering the main loop within
 * the worker thread, notifying the main thread it is safe to continue.
 */
struct StartControlAck
{
}

/**
 * A QuitControlMessage is sent to a Worker to ask it to shutdown
 * operations.
 */
struct QuitControlMessage
{
    /**
     * Request a QuitControlAck response (blocking)
     */
    bool blocking = true;
}

/**
 * A QuitControlAck is sent in response to a blocking QuitControlMessage
 */
struct QuitControlAck
{
}

/**
 * A PerformWorkControlMessage is sent to a Worker to ask it to try performing
 * the main work unit.
 */
struct PerformWorkControlMessage
{
    /**
     * Request a PerformWorkControlAck response (blocking)
     */
    bool blocking = true;
}

/**
 * A PerformWorkControlAck is sent in response to a blocking PerformWorkControlMessage
 */
struct PerformWorkControlAck
{
}

/**
 * A SyncControlMessage is sent to a Worker to ask it sync completed work
 */
struct SyncControlMessage
{
    /**
     * Request a SyncControlAck response (blocking)
     */
    bool blocking = true;
}

/**
 * A SyncControlAck is sent in response to a blocking SyncControlMessage
 */
struct SyncControlAck
{
}

/**
 * A SyncControlMessage is sent to a Worker to ask it to allocate work to processor
 */
struct AllocateWorkControlMessage
{
    /**
     * Request a AllocateWorkControlAck response (blocking)
     */
    bool blocking = true;
}

/**
 * A AllocateWorkControlAck is sent in response to a blocking AllocateWorkControlMessage
 */
struct AllocateWorkControlAck
{
}
