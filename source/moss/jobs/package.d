/*
 * This file is part of moss-jobs.
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

/**
 * The moss job system and main loop APIs
 */
module moss.jobs;

public import moss.jobs.idleprocessor;
public import moss.jobs.jobsystem;
public import moss.jobs.mainloop;
public import moss.jobs.processorgroup;
public import moss.jobs.systemprocessor;

/* Private build time imports */
import moss.jobs.messaging;
