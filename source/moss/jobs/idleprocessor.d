/*
 * This file is part of moss-jobs.
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.jobs.idleprocessor;

public import moss.jobs.systemprocessor;
public import std.stdint : uint64_t;

import core.sync.mutex;
import std.conv : to;
import std.exception : enforce;
import std.algorithm : canFind, remove, filter, each, map;
import std.array : array;

/** 
 * Each IdleCallback returns CallbackControl to determine whether we continue
 * running in the next loop, or remove the callback now
 */
public enum CallbackControl
{
    /**
     * The callback will be removed from further execution
     */
    Stop = 0,

    /**
     * Continue with this callback in the execution queue
     */
    Continue,
}
/**
 * IdleCallback can be added to the main loop to run during the next tick
 * duration.
 */
public alias IdleCallback = CallbackControl delegate();

/**
 * Helper wrapper to run a callback once without needing to return Stop
 */
public alias NonRecurringIdleCallback = void delegate();

/**
 * Each Callback gets an ID assigned
 */
public alias CallbackID = uint64_t;

/**
 * Internal wrapper for callbacks
 */
private struct CallbackPacket
{
    IdleCallback callback;
    CallbackID id;
}
/**
 * The IdleProcessor will attempt to run some sequential set of events in the
 * idle time.
 */
final class IdleProcessor : SystemProcessor
{
    /**
     * Construct a new IdleProcessor. In future this will only be allowed by the
     * EventLoop
     */
    this()
    {
        super("idleProcessor", ProcessorMode.Main);
        idMutex = new shared Mutex();
        accessMutex = new shared Mutex();
    }

    /**
     * We can work if we have more than 1 callback
     */
    override bool allocateWork()
    {
        return callbacks.length > 0;
    }

    /**
     * Upon run, we run through the live callback list and run every callback
     * If a callback returns CallbackControl.Stop, we'll remove it from further
     * iterations so it only runs once. Otherwise, it will run forever.
     */
    override void performWork()
    {
        import std.stdio : writeln;

        bool hasRemovals = false;
        CallbackID[] tombstoned;

        /* Run all callbacks, marking removals if needed */
        synchronized (accessMutex)
        {
            foreach (ref cbw; callbacks)
            {
                /* Removable? Remove it. */
                if (cbw.callback !is null && cbw.callback() == CallbackControl.Stop)
                {
                    cbw.callback = null;
                    hasRemovals = true;
                }
            }

            if (!hasRemovals)
            {
                return;
            }

            /* Filter out the tombstones out of loop */
            auto tombstonedSet = callbacks.filter!((ref cb) => cb.callback is null)
                .map!((ref cb) => cb.id);
            if (tombstonedSet.empty)
            {
                return;
            }
            tombstoned = tombstonedSet.array();
        }

        /* For all removables, wipe them out locksafe */
        tombstoned.each!((removal) => removeCallback(removal));
    }

    /**
     * Stopping the idle processor will clear everything from the execution queue
     */
    override void stop()
    {
        synchronized (accessMutex)
        {
            synchronized (idMutex)
            {
                callbacks = [];
                clearIDs = [];
                nextAvailID = 0;
            }
        }
    }

    /**
     * No need to sync
     */
    override void syncWork()
    {
    }

    /**
     * Simpler helper for one off (non recurring) idle callbacks
     */
    CallbackID addCallback(NonRecurringIdleCallback cb)
    {
        return addCallback(() => { cb(); return CallbackControl.Stop; }());
    }

    /**
     * Add a callback to the execution queue and return a CallbackID
     * so that it can be removed.
     */
    CallbackID addCallback(IdleCallback cb)
    {
        synchronized (accessMutex)
        {
            auto cbPacket = CallbackPacket(cb, 0);
            cbPacket.id = nextCallbackID();
            enforce(cb !is null, "addCallback(): Cannot add null callback");
            callbacks ~= cbPacket;

            return cbPacket.id;
        }
    }

    /**
     * Remove a callback from idle processing by its CallbackID
     */
    void removeCallback(CallbackID id)
    {
        synchronized (accessMutex)
        {
            enforce(callbacks.canFind!((c) => c.id == id),
                    "IdleProcessor.removeCallback(): Cannot find ID " ~ to!string(id));
            callbacks = callbacks.remove!((c) => c.id == id);
            clearIDs ~= id;
        }
    }

private:

    /**
     * Generate a new callback ID atomically. If an old removed ID is available,
     * return that first.
     */
    CallbackID nextCallbackID()
    {
        idMutex.lock_nothrow();
        scope (exit)
        {
            idMutex.unlock_nothrow();
        }

        if (clearIDs.length > 0)
        {
            auto ret = clearIDs[0];
            clearIDs = clearIDs[1 .. $];
            return ret;
        }
        ++nextAvailID;
        return nextAvailID;
    }

    CallbackPacket[] callbacks;
    CallbackID[] clearIDs = [];
    CallbackID nextAvailID = 0;
    shared Mutex idMutex = null;
    shared Mutex accessMutex = null;
}
